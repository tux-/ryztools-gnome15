#!/usr/bin/perl

use strict;
use warnings;
use XML::Simple qw(:strict);
use File::Fetch;
use File::stat;
use Data::Dumper;
use Switch;
use Time::HiRes qw(gettimeofday);
use Time::Piece;
use POSIX "fmod";
use POSIX qw(strftime locale_h);

# Load in the Net::DBus core module.
use Net::DBus;

use constant HOUR_TICKS => 1800;
use constant DAY_HOURS => 24;
use constant SEASON_DAYS => 90;
use constant MONTH_DAYS => 30;
use constant CYCLE_MONTHS => 12;
use constant JY_CYCLES => 4;
use constant WEEK_DAYS => 6;
# 0 = spring, 1 = summer, 2 = autumn, 3 = winter
use constant CYCLE_SEASON => 4;

use constant SEASON_OFFSET_TICKS => 61 * DAY_HOURS * HOUR_TICKS;

use constant START_JY => 2568;

# Seems like the tick runs about five - eight minutes slow during a season.
# So we need to add five minutes. (Better to be early than late).
use constant SEASON_MINUTES_OFFSET => 5;

# Helpers
use constant CYCLE_DAYS => CYCLE_MONTHS * MONTH_DAYS;
use constant JY_DAYS => CYCLE_DAYS * JY_CYCLES;
use constant JY_MONTHS => CYCLE_MONTHS * JY_CYCLES;
use constant SEASON_TICKS => HOUR_TICKS * DAY_HOURS * SEASON_DAYS;
use constant JY_TICKS => SEASON_TICKS * JY_CYCLES;

my $url = "http://api.ryzom.com/time.php?format=xml";
my $fileName = "/tmp/time.xml/time.php";

sub getTick {
	my $serverTick = shift(@_);
	my $created = shift(@_);
	my $now = (gettimeofday * 1000);
	my $createdOffset = (($now - ($created * 1000)) / 1000);
	my $correctedTick = $serverTick + ($createdOffset * 10);
	return $correctedTick;
}

sub getDayOfSeason {
	my $serverTick = shift(@_);
	my $created = shift(@_);
	my $timeInHours = (getTick($serverTick, $created) - SEASON_OFFSET_TICKS) / HOUR_TICKS;
	my $day = $timeInHours / DAY_HOURS;
	return int(fmod($day, SEASON_DAYS));
}

sub getTimeOfDay {
	my $serverTick = shift(@_);
	my $created = shift(@_);
	my $timeInHours = getTick($serverTick, $created) / HOUR_TICKS;
	return abs(fmod($timeInHours, DAY_HOURS));
}

setlocale(LC_ALL, "en_GB.utf8");

# Get the session object
my $bus=Net::DBus->session();

# Connect to the service
my $service=$bus->get_service("org.gnome15.Gnome15");

#Get the service object
my $g19=$service->get_object("/org/gnome15/Service", "org.gnome15.Service");

# Just dump some server information
my @xresult;
@xresult=$g19->GetServerInformation();
print "ServerInformation: " . join('|', @xresult) . "\n";

# Try to connect to the first available screen (and hope it's a G19)
@xresult=$g19->GetScreens();
my $g19screenname = $xresult[0]->[0];
print "Using screen $g19screenname\n";
my $g19screen = $service->get_object($g19screenname, "org.gnome15.Screen");

# Now, create a page and get its handle
my $pagename = $g19screen->CreatePage('Ryztools', 'Ryztools', 50);
#print $pagename;
my $page = $service->get_object($pagename, "org.gnome15.Page");

sleep(1);

# The xml should be validated before stored.
my $ff = File::Fetch->new(uri => $url);
my $where = $ff->fetch(to => '/tmp/time.xml');

unless (-e $fileName) {
	print "No time xml file.\n";
	exit 1;
}

my $count = 0;
while ($count <= 1000) {
	if ($count == 300) {
		$count = 0;

		# The xml should be validated before stored.
		my $ff = File::Fetch->new(uri => $url);
		my $where = $ff->fetch(to => '/tmp/time.xml');

		print "==== Fetching brand new time.xml ====\n";
	}

	my $xml = XMLin($fileName, KeyAttr => { }, ForceArray => [ ]);
	my $serverTick = $xml->{server_tick};
	my $created = $xml->{cache}->{created};

	# Create a drawing surface
	$page->NewSurface();

	my $tickOfDay = int(getTick($serverTick, $created) % (HOUR_TICKS * DAY_HOURS));

	my $h = int($tickOfDay / HOUR_TICKS);
	my $tod = "Night";
	# Dawn : 3h-6h = 9 min IRL
	# Day : 6h-20 = 42 min IRL
	# Twilight : 20h-22h = 6 min IRL
	# Night : 22h-3h = 15 min IRL
	if ($h >= 22) {
		$tod = "Night";
	} elsif ($h >= 20) {
		$tod = "Twilight";
	} elsif ($h >= 6) {
		$tod = "Day";
	} elsif ($h >= 3) {
		$tod = "Dawn";
	}

	my $ig_h = sprintf("%02s", $h);
	my $ig_m = sprintf("%02s", int(fmod($tickOfDay, HOUR_TICKS) / 30));

	my $apiMinutesLeftInSeason = 6480 - (getDayOfSeason($serverTick, $created) * 72) - ((getTimeOfDay($serverTick, $created) * 3));
	$apiMinutesLeftInSeason += (($apiMinutesLeftInSeason / 6480) * SEASON_MINUTES_OFFSET);

	my $total_secsToSeasonChange = int($apiMinutesLeftInSeason * 60);
	my $apiNextSeasonChange = gettimeofday + $total_secsToSeasonChange;

	my $date = localtime($apiNextSeasonChange)->strftime('%F %T');
	# print $date . "\n";

	my $d_t = int($total_secsToSeasonChange / 86400);
	my $h_t = int($total_secsToSeasonChange % 86400 / 3600);
	my $m_t = int($total_secsToSeasonChange % 3600 / 60);
	my $s_t = int($total_secsToSeasonChange % 3600 % 60);
	# print "$d_t $h_t $m_t $s_t";

	my $season = "Spring";
	switch ($xml->{season}) {
		case "1" {
			$season = "Summer";
		}
		case "2" {
			$season = "Autumn";
		}
		case "3" {
			$season = "Winter";
		}
	}
	# print "Ryztools\n$ig_h:$ig_m\n$tod\n$season";

	$page->SetFont("8", "Fixed", "normal", "normal");
	$page->Text("Time\n$ig_h:$ig_m\n$tod\n$season", 10, 0, 150, 40, "center,top,wrapword");
	# $page->SetFont("6", "Fixed", "normal", "normal");
	$date = localtime($apiNextSeasonChange)->strftime("%A\n%e %b\n%H:%M");
	$page->Text("Ryztools\n$date", 0, 0, 160, 40, "left,top,wrapword");
	$page->Text("$d_t d\n$h_t h\n$m_t m\n$s_t s", 0, 0, 160, 40, "right,top,wrapword");
	# Update screen
	$page->DrawSurface();
	$page->Redraw();
	$count++;
	sleep(1);
}

# Cleanup (delete the page)
$page->Delete();

exit 0;
